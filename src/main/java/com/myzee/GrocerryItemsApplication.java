package com.myzee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class GrocerryItemsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrocerryItemsApplication.class, args);
	}

}
