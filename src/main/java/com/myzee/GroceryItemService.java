package com.myzee;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/grocery")
public class GroceryItemService {
	
	@RequestMapping("/deliver")
	public ItemsList deliver() {
		Item item1 = new Item("Radish", 60);
		Item item2 = new Item("Bottle gourd", 50);
		
		List<Item> l = new ArrayList<>();
		l.add(item1);
		l.add(item2);
		
		
		
		return new ItemsList(l);
//		return item1.toString() + item2.toString();
	}
}
