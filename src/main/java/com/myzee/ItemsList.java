package com.myzee;

import java.util.List;

public class ItemsList {
	private List<Item> itemList;
	
	public ItemsList() {
		// TODO Auto-generated constructor stub
	}
	
	public ItemsList(List<Item> itemList) {
		super();
		this.itemList = itemList;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	
}
